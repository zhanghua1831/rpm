Name:          rpm
Version:       4.15.1
Release:       21
Summary:       RPM Package Manager
License:       GPLv2+
URL:           http://www.rpm.org/
Source0:       http://ftp.rpm.org/releases/rpm-4.15.x/%{name}-%{version}.tar.bz2

Patch1: Unbundle-config-site-and-add-RPM-LD-FLAGS-macro.patch
Patch2: rpm-4.12.0-rpm2cpio-hack.patch
Patch3: find-debuginfo.sh-decompress-DWARF-compressed-ELF-se.patch
Patch4: skip-updating-the-preferences.patch
Patch5: add-dist-to-release-by-default.patch
Patch6: Silence-spurious-error-message-from-lsetfilecon-on-E.patch
Patch7: revert-always-execute-file-trigger-scriptlet-callbac.patch
Patch8: change-rpmsigdig-test-s-SHA256HEADER-SHA1HEADER-SIGM.patch

Patch9: bugfix-rpm-4.11.3-add-aarch64_ilp32-arch.patch
Patch10: bugfix-rpm-4.14.2-fix-tty-failed.patch
Patch11: bugfix-rpm-4.14.2-wait-once-get-rpmlock-fail.patch
Patch12: Use-common-error-logic-regardless-of-setexecfilecon-.patch
Patch13: Generate-digest-lists.patch
Patch14: Add-digest-list-plugin.patch
Patch15: Don-t-add-dist-to-release-if-it-is-already-there.patch
Patch16: Use-user.digest_list-to-avoid-duplicate-processing-o.patch
Patch17: call-process_digest_list-after-files-are-added.patch

Patch18: backport-Fix-changelog-trimming-to-work-relative-to-newest-ex.patch
Patch19: backport-Fix-resource-leaks-on-zstd-open-error-paths.patch
Patch20: backport-rpmio-initialise-libgcrypt.patch
Patch21: backport-fix-zstd-magic.patch
Patch22: backport-Don-t-require-signature-header-to-be-in-single-conti.patch
Patch23: backport-ndb-only-clear-the-dbenv-in-the-rpmdb-if-the-last-re.patch
Patch24: backport-Fix-regression-on-v3-package-handling-on-database-re.patch
Patch25: backport-Fix-a-minor-memory-leak-on-suppressed-inhibition-loc.patch
Patch26: backport-Fix-POPT_ARG_STRING-memleaks-in-librpmbuild.patch
Patch27: backport-Fix-build-regression-in-commit-307872f71b357a3839fd0.patch
Patch28: backport-Fix-isUnorderedReq-for-multiple-qualifiers.patch
Patch29: backport-If-fork-fails-in-getOutputFrom-close-opened-unused-p.patch
Patch30: backport-Fix-pointer-dereference-before-testing-for-NULL-in-r.patch
Patch31: backport-Don-t-look-into-source-package-provides-in-depsolvin.patch
Patch32: backport-rpmfiArchiveRead-use-signed-return-value-to-handle-1.patch
Patch33: backport-Fix-bump-up-the-limit-of-signature-header-to-64MB.patch
Patch34: backport-Remove-compare-of-global-array-tagsByName-to-NULL.patch
Patch35: backport-Always-close-libelf-handle-1313.patch
Patch36: backport-Add-missing-terminator-to-copyTagsFromMainDebug-arra.patch
Patch37: backport-Fix-possible-read-beyond-buffer-in-rstrnlenhash.patch
Patch38: backport-Make-fdSeek-return-0-on-success-1-on-error.patch
Patch39: backport-Fix-logic-error-in-grabArgs.patch
Patch40: backport-Use-libelf-for-determining-file-colors.patch

BuildRequires: gcc autoconf automake libtool make gawk popt-devel openssl-devel readline-devel libdb-devel 
BuildRequires: zlib-devel libzstd-devel xz-devel bzip2-devel libarchive-devel ima-evm-utils-devel
BuildRequires: dbus-devel fakechroot elfutils-devel elfutils-libelf-devel ima-evm-utils
BuildRequires: lua-devel libcap-devel libacl-devel libselinux-devel file-devel gettext-devel ncurses-devel
BuildRequires: system-rpm-config gdb dwz
Requires:      coreutils popt curl zstd libcap gnupg2 crontabs logrotate libdb-utils %{name}-libs
Obsoletes:     %{name}-build-libs %{name}-sign-libs %{name}-sign %{name}-cron
Provides:      %{name}-build-libs %{name}-sign-libs %{name}-sign %{name}-cron
Obsoletes:     %{name}-plugin-selinux %{name}-plugin-syslog %{name}-plugin-systemd-inhibit %{name}-plugin-ima %{name}-plugin-prioreset
Provides:      %{name}-plugin-selinux %{name}-plugin-syslog %{name}-plugin-systemd-inhibit %{name}-plugin-ima %{name}-plugin-prioreset

%description
The RPM Package Manager (RPM) is a powerful package management system capability as below

-building computer software from source into easily distributable packages
-installing, updating and uninstalling packaged software
-querying detailed information about the packaged software, whether installed or not
-verifying integrity of packaged software and resulting software installation

%package libs
Summary:     Shared library of rpm 4.15

%description libs
Shared library of rpm 4.15.

%package build
Summary:     Scripts and executable programs used to build packages
Requires:    %{name} = %{version}-%{release}
Requires:    elfutils binutils findutils sed grep gawk diffutils file patch
Requires:    tar unzip gzip bzip2 cpio xz zstd pkgconfig system-rpm-config
Requires:    gcc-c++ autoconf automake binutils gcc gdbm gettext glibc
Requires:    gdb-headless ncurses perl make git chrpath

%description build
This package provides scripts and executable programs that used to
build rpm packages.

%package -n python3-%{name}
Summary: Python3 bindings for RPM user
BuildRequires: python3-devel
%{?python_provide:%python_provide python3-%{name}}
Requires: %{name} = %{version}-%{release}
Provides: %{name}-python3 = %{version}-%{release}
Obsoletes: %{name}-python3 < %{version}-%{release}

%description -n python3-%{name}
This package contains a module that allow applications
written with Python3 to use the interface
supplied by RPM.

%package devel
Summary:  Development files for RPM handling
Requires: %{name} = %{version}-%{release}
Requires: popt-devel

%description devel
%{summary}.

%package help
Summary: Man page for %{name}
BuildArch: noarch
Obsoletes: apidocs

%description help
%{summary}.

%prep
%autosetup -n %{name}-%{version} -p1

sed -ie 's:^python test:python2 test:g' tests/rpmtests tests/local.at

%build
CPPFLAGS="$CPPFLAGS -DLUA_COMPAT_APIINTCASTS"
CFLAGS="$RPM_OPT_FLAGS -DLUA_COMPAT_APIINTCASTS"
LDFLAGS="$LDFLAGS %{?__global_ldflags}"
export CPPFLAGS CFLAGS LDFLAGS

autoreconf -i -f

for i in $(find . -name ltmain.sh) ; do
     %{__sed} -i.backup -e 's~compiler_flags=$~compiler_flags="%{_hardened_ldflags}"~' $i
done;

./configure \
    --prefix=%{_usr} \
    --sysconfdir=%{_sysconfdir} \
    --localstatedir=%{_var} \
    --sharedstatedir=%{_var}/lib \
    --libdir=%{_libdir} \
    --build=%{_target_platform} \
    --host=%{_target_platform} \
    --with-vendor=%{_vendor} \
    --with-external-db \
    --with-lua \
    --with-selinux \
    --with-cap \
    --with-acl \
    --with-imaevm \
    --enable-zstd \
    --enable-python \
    --with-crypto=openssl

%make_build

pushd python
%{__python3} setup.py build
popd

%install
%make_install

pushd python
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
popd

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily
install -m 755 scripts/rpm.daily ${RPM_BUILD_ROOT}%{_sysconfdir}/cron.daily/rpm

mkdir -p ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d
install -m 644 scripts/rpm.log ${RPM_BUILD_ROOT}%{_sysconfdir}/logrotate.d/rpm

mkdir -p ${RPM_BUILD_ROOT}/usr/lib/tmpfiles.d
echo "r /var/lib/rpm/__db.*" > ${RPM_BUILD_ROOT}/usr/lib/tmpfiles.d/rpm.conf

mkdir -p $RPM_BUILD_ROOT%{_sysconfdir}/rpm
mkdir -p $RPM_BUILD_ROOT%{_rpmconfigdir}/macros.d
mkdir -p $RPM_BUILD_ROOT/var/lib/rpm

for dbi in \
    Basenames Conflictname Dirnames Group Installtid Name Obsoletename \
    Packages Providename Requirename Triggername Sha1header Sigmd5 \
    __db.001 __db.002 __db.003 __db.004 __db.005 __db.006 __db.007 \
    __db.008 __db.009
do
    touch $RPM_BUILD_ROOT/var/lib/rpm/$dbi
done

#./rpmdb --dbpath=$RPM_BUILD_ROOT/var/lib/rpm --initdb

for dbutil in dump load recover stat upgrade verify
do
    ln -s ../../bin/db_${dbutil} $RPM_BUILD_ROOT/usr/lib/rpm/rpmdb_${dbutil}
done

%find_lang %{name}

find $RPM_BUILD_ROOT -name "*.la"|xargs rm -f

rm -f $RPM_BUILD_ROOT/%{_rpmconfigdir}/{perldeps.pl,perl.*,pythond*}
rm -f $RPM_BUILD_ROOT/%{_fileattrsdir}/{perl*,python*}
rm -f $RPM_BUILD_ROOT/%{_rpmconfigdir}/{tcl.req,osgideps.pl}

%check
make check || (cat tests/rpmtests.log; exit 0)

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%files -f %{name}.lang
%defattr(-,root,root)
%license COPYING
%doc CREDITS
/usr/lib/tmpfiles.d/rpm.conf
%{_sysconfdir}/cron.daily/rpm
%config(noreplace) %{_sysconfdir}/logrotate.d/rpm
%dir %{_sysconfdir}/rpm
%dir /var/lib/rpm
%attr(0644, root, root) %verify(not md5 size mtime) %ghost %config(missingok,noreplace) /var/lib/rpm/*
%lang(fr) %{_mandir}/fr/man[18]/*.[18]*
%lang(ko) %{_mandir}/ko/man[18]/*.[18]*
%lang(ja) %{_mandir}/ja/man[18]/*.[18]*
%lang(pl) %{_mandir}/pl/man[18]/*.[18]*
%lang(ru) %{_mandir}/ru/man[18]/*.[18]*
%lang(sk) %{_mandir}/sk/man[18]/*.[18]*

%attr(0755, root, root) %dir %{_rpmconfigdir}
%{_rpmconfigdir}/macros
%{_rpmconfigdir}/macros.d
%{_rpmconfigdir}/rpmpopt*
%{_rpmconfigdir}/rpmrc
%{_rpmconfigdir}/rpmdb_*
%{_rpmconfigdir}/rpm.daily
%{_rpmconfigdir}/rpm.log
%{_rpmconfigdir}/rpm.supp
%{_rpmconfigdir}/rpm2cpio.sh
%{_rpmconfigdir}/tgpg
%{_rpmconfigdir}/platform
%{_libdir}/rpm-plugins/
%dir %{_rpmconfigdir}/fileattrs
%{_bindir}/rpm
%{_bindir}/rpm2archive
%{_bindir}/rpm2cpio
%{_bindir}/rpmdb
%{_bindir}/rpmkeys
%{_bindir}/rpmquery
%{_bindir}/rpmverify
%{_bindir}/rpmsign

%files libs
%{_libdir}/librpm*.so.9*

%files build
%defattr(-,root,root)
%{_bindir}/rpmbuild
%{_bindir}/gendiff
%{_bindir}/rpmspec

%{_rpmconfigdir}/brp-*
%{_rpmconfigdir}/check-*
%{_rpmconfigdir}/debugedit
%{_rpmconfigdir}/sepdebugcrcfix
%{_rpmconfigdir}/find-debuginfo.sh
%{_rpmconfigdir}/find-lang.sh
%{_rpmconfigdir}/*provides*
%{_rpmconfigdir}/*requires*
%{_rpmconfigdir}/*deps*
%{_rpmconfigdir}/*.prov
%{_rpmconfigdir}/*.req
%{_rpmconfigdir}/config.*
%{_rpmconfigdir}/mkinstalldirs
%{_rpmconfigdir}/fileattrs/*

%files -n python3-%{name}
%defattr(-,root,root)
%{python3_sitearch}/%{name}/
%{python3_sitearch}/%{name}-%{version}*.egg-info

%files devel
%defattr(-,root,root)
%{_bindir}/rpmgraph
%{_libdir}/librp*[a-z].so
%{_libdir}/pkgconfig/%{name}.pc
%{_includedir}/%{name}/

%files help
%defattr(-,root,root)
%doc doc/manual/[a-z]*
%doc doc/librpm/html/*
%{_mandir}/man8/rpm*.8*
%{_mandir}/man1/gendiff.1*

%changelog
* Mon Jan 11 2021 Liquor <lirui130@huawei.com> - 4.15.1-21
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:backport patches from upstream

* Thu Dec 17 2020 Anakin Zhang <benjamin93@163.com> - 4.15.1-20
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:call process_digest_list before files are added

* Thu Dec 17 2020 Anakin Zhang <benjamin93@163.com> - 4.15.1-19
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:use user.digest_list to avoid duplicate processing of the digest lists

* Thu Oct 29 2020 Liquor <lirui130@huawei.com> - 4.15.1-18
- Type:requirement
- ID:NA
- SUG:NA
- DESC:remove python2

* Tue Jul 14 2020 Roberto Sassu <roberto.sassu@huawei.com> - 4.15.1-17
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add support for digest lists

* Fri May 22 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-16
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:solve the error of setexecfilecon

* Wed May 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-15
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:solve the failure of rpmsigdig.at test

* Mon Mar 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-14
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add requires of some common build packages to rpm-build

* Wed Mar 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert always execute file trigger scriptlet callbacks with owning header

* Fri Mar 6 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:silence spurious error message from lsetfilecon()

* Wed Mar 4 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-11
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add dist to release by default

* Mon Mar 2 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:revert last commit

* Thu Feb 27 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add dist to the name of package

* Fri Feb 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:skip update the preference of gpg during make check

* Mon Jan 20 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete unneeded obsoletes

* Sat Jan 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:delete unneeded shared library

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:change requires to build requires

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add build requires of ima-evm-utils and old shared library

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add requires of shared library

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add subpack of librpm8 and librpm9 to support update

* Fri Jan 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 4.15.1-1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:update version to 4.15.1

* Tue Dec 24 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.14.2-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:bugfix in files

* Wed Nov 13 2019 hexiaowen<hexiaowen@huawei.com> - 4.14.2-4
- add system-rpm-config buildrequires

* Fri Sep 20 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.14.2-3
- Delete redundant information

* Mon Sep 09 2019 openEuler Buildteam <buildteam@openeuler.org> - 4.14.2-2
- Package init
